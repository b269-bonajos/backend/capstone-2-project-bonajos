const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  active: {
    type: Boolean,
    default: true
  },
  purchasedOn: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model("Order", orderSchema);



