const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const productController = require("../controllers/productController");

const auth = require("../auth");


// Route for creating product
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all active product
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product
router.put("/:productId", (req,res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for archive product
router.put("/:productId/archive", (req,res) => {
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});







module.exports = router;



