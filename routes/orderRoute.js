const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const auth = require("../auth");
const orderController = require("../controllers/orderController");

router.post("/checkout", auth.verify, (req, res) => {
  const data = {
    order: req.body,
    userId: auth.decode(req.headers.authorization).Id,
    productId: req.body.productId,
    name: req.body.name,
    quantity: req.body.quantity
  }
  orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;


