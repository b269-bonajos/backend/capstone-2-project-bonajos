const User = require("../models/User");
const Order = require("../models/Order");
const auth = require("../auth");




module.exports.createOrder = async (data) => {
  if (data.isAdmin) {
    return "For user only";
  }

  let newOrder = new Order({
    userId: data.userId,
    orderId: [{
      productId: data.order.productId,
      name: data.order.name,
      quantity: data.order.quantity,
      price: data.order.price
    }]
  });

  return newOrder.save().then((order) => {
    return newOrder
  });
};


