const User = require("../models/User");

const bcrypt = require("bcrypt");

const auth = require("../auth");
const Product = require("../models/Product");


module.exports.registerUser = (reqBody) => {
  return User.findOne({ $or: [{ email: reqBody.email }, { mobileNo: reqBody.mobileNo }] })
    .then((existingUser) => {
      if (existingUser) {
        return ("User already exists");
      } else {
        let newUser = new User({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          mobileNo: reqBody.mobileNo,
          password: bcrypt.hashSync(reqBody.password, 10)
        });
        return newUser.save().then(() => {
          return ({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          mobileNo: reqBody.mobileNo,
          password: bcrypt.hashSync(reqBody.password, 10)
        });
        });
      }
    });
};



module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		// User does not exist
		if (result == null) {
			return false
			// User exists
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};
